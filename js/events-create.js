var isAdminLoggedIn = true;

if (!isAdminLoggedIn) {
    $("#eventFormContainer").hide();
    $("#eventsNotLoggedIn").css('visibility', 'visible');
}

function showModalFailCreationEvent(text) {
    $("#modalText").text(text);
    $("#img-ok").hide();
    $("#img-no").show();
    $("#modalEventFail").modal('show');
}

function showModalSuccessCreateEvent(text) {
    $("#modalText").text(text);
    $("#img-ok").show();
    $("#img-no").hide();
    $("#modalEventFail").modal('show');
}

function nextIdForEvents(existingEvents) {
    var max = 0;
    var keys = Object.keys(existingEvents);
    for (var i = 0; i < keys.length; i++) {
        if (keys[i] > max) {
            max = keys[i];
        }
    }
    return max + 1;
}

function createEvent(title, date, location, description) {
    var localStorageEvents = localStorage['events'];
    var existingEvents = {};
    if (localStorageEvents !== undefined) {
        existingEvents = JSON.parse(localStorageEvents);
    }

    var id;
    if (eventExisting(eventId)) {
        id = eventId
    } else {
        id = nextIdForEvents(existingEvents);
    }

    var newEvent = {
        title: title,
        date: date,
        location: location,
        description: description
    };

    existingEvents[id] = newEvent;

    localStorage['events'] = JSON.stringify(existingEvents);
    showModalSuccessCreateEvent("Event created successfully");

    if (!eventExisting(eventId)) {
        // CLEAR
        $("#txtEventTitle").val("");
        $("#txtEventDate").val("");
        $("#txtEventLocation").val("");
        $("#txtEventDescription").val("");

    }
}

function handleNewEvent() {
    // grab input
    var title = $("#txtEventTitle").val();
    if (title.trim() === "") {
        showModalFailCreationEvent("Please input a title");
        return;
    }

    var date = $("#txtEventDate").val();
    if (date.trim() === "") {
        showModalFailCreationEvent("Please input a date");
        return;
    }

    var location = $("#txtEventLocation").val();
    if (location.trim() === "") {
        showModalFailCreationEvent("Please select a location");
        return;
    }

    var description = $("#txtEventDescription").val();
    if (description.trim() === "") {
        showModalFailCreationEvent("Please input a description");
        return;
    }

    createEvent(title, date, location, description);
}

$(document).on('click', '#btnCreateEvent', function () {
    handleNewEvent();
});

$("#txtEventDate").datetimepicker({
    format: DATE_FORMAT
});

var eventId = getParam("event");

function fillFormWithExistingEvent(eventId) {
    var localStorageEvents = localStorage['events'];
    var existingEvents = {};
    if (localStorageEvents !== undefined) {
        existingEvents = JSON.parse(localStorageEvents);
    }
    var event = existingEvents[eventId];

    $("#txtEventTitle").val(event.title);
    $("#txtEventDate").val(event.date);
    $("#txtEventLocation").val(event.location);
    $("#txtEventDescription").val(event.description);
}


function eventExisting(eventId) {
    return (eventId !== "" && eventId !== undefined)
}

function deleteEvent(eventId) {
    var localStorageEvents = localStorage['events'];
    var existingEvents = {};
    if (localStorageEvents !== undefined) {
        existingEvents = JSON.parse(localStorageEvents);
    }

    delete existingEvents[eventId];
    localStorage['events'] = JSON.stringify(existingEvents);
    window.location = "events-create.html"
}

if (eventExisting(eventId)) {
    $(document).on("click", "#btnDeleteEvent", function () {
        deleteEvent(eventId);
    });
    $("#btnDeleteEvent").show();
    fillFormWithExistingEvent(eventId);
}

if (!userLoggedIn()) {
    window.location = "member_login.html";
}