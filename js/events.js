var HTML_EVENT_TEMPLATE = "<div class=\"flex-parent col-lg-12 text-center\">\
    <h2>$TITLE <span class=\"edit-span\" data-eventid=\"$ID\">edit</span>\
<br>\
<small>$DATE</small>\
<br>\
<small>$LOCATION</small>\
</h2>\
<p style=\"display: none\" class=\"description\">$DESCRIPTION</p>\
<button class=\"btn btn-default btn-read-more\">Read More</button>\
<hr>\
</div>";

var EVENTS_CONTAINER = $("#eventsContainer");

function include(thisEvent) {
    var theDate = thisEvent.date.split(" ")[0];
    var eventDate = moment(theDate, DATE_FORMAT);
    var today = moment().startOf('day');
    if (getParam("past") === "true") {
        return eventDate.isSameOrBefore(today);
    }
    return eventDate.isSameOrAfter(today);
}

function displayEvents() {
    var localStorageEvents = localStorage['events'];
    var existingEvents = [];
    if (localStorageEvents !== undefined) {
        existingEvents = JSON.parse(localStorageEvents);
    }
    for (var i = 0; i < Object.keys(existingEvents).length; i++) {
        var thisEventKey = Object.keys(existingEvents)[i];
        var thisEvent = existingEvents[thisEventKey];
        if (include(thisEvent)) {

            var htmlEvent = HTML_EVENT_TEMPLATE
                .replace("$TITLE", thisEvent.title)
                .replace("$DATE", thisEvent.date)
                .replace("$LOCATION", thisEvent.location)
                .replace("$DESCRIPTION", thisEvent.description)
                .replace("$ID", thisEventKey);

            EVENTS_CONTAINER.append(htmlEvent);
        }
    }

    if (!userLoggedIn()) {
        $(".edit-span").hide();
    }
}

function displayDescriptionOfEventFromButton(readMoreBtnRef) {
    readMoreBtnRef.style.display = "none";
    readMoreBtnRef.previousSibling.style.display = "inherit";
}

$(document).on("click", ".btn-read-more", function () {
    displayDescriptionOfEventFromButton(this);
});

$(document).on("click", ".edit-span", function() {

console.log("create");
   var eventId = this.dataset["eventid"];
   window.location = "events-create.html?event=" + eventId;
});

displayEvents();
