function checkSession() {
//check if key "logIn" in localstorage exists
    if (localStorage.getItem("logIn") === null) {
        //change create event href and link text to member log in href and Log In text
        $("#a-create-event").attr("href", "member_login.html");
        $("#a-create-partner").attr("href", "member_login.html");
        $(".btn-edit").hide();
        
        $("#logoutMember").attr("href", "member_login.html").attr("id", "member").text("Member Log In");
    }

    else {
        //change "Member login" href to logout and text to "Log Out", because user is logged in
        $("#member").attr("href", "#").attr("id", "logoutMember").text("Log Out");
         $(".btn-edit").show();
    }
};
