function getParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

function userLoggedIn() {
    if (localStorage["logIn"] === undefined) {
        return false;
    }
    return true;
}

var DATE_FORMAT = "DD/MM/YYYY h:mm a";
