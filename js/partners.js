$( document ).ready(function() {

	$('#btnAdd').click(function(){
	getPartner();

	});
	function deleteValueInputs(){
	  $('.companiesNames').val('')
	     $('#NameTxt').val('');
	      $('#emailTxt').val('');
	      $('.eventsTxt').val('');
	      $('#logoTxt').val('');
	}

	function getPartner(){
	//Company name
	var companyName = $('.companiesNames option:selected').text();
	//Contact person name
	var personName = $('#NameTxt').val();
	//Company email
	var companyEmail = $('#emailTxt').val();
	//Events
	var eventsNames = $('.eventsTxt option:selected').text();
	//Select a logo
	var logo = $('#logoTxt option:selected').text();
			//checks for empty input fields in the form
	var empty = $(".flex-item").parent().find("input").filter(function() {
				return this.value === "";
	});
	if(companyName == logo && !empty.length){
		var jData = getDataFromLocalStorageAsObject();	

		if(jData == ""){
				var partners = {};
				partners.id = 1;
				partners.name = companyName;
				partners.contactname = personName;
				partners.email = companyEmail;
				partners.ev = eventsNames;
				partners.img = logo+".jpg";
				//console.log(partners);
				var aPartners =  getDataFromLocalStorageAsObject();	
				aPartners.push(partners);
				var sFinal = JSON.stringify(aPartners);
				//console.log(sFinal);	
				localStorage.sapartners = sFinal;
				 deleteValueInputs();
				 $("#img-no").hide();
            	$("#img-ok").show();
			    showModalFail('Partner was created successfully!');
			}
			else{
		
		  var max = 1;
	  
	   
			for (var i = 0; i < jData.length; i++){
	           if (jData[i].id > max) {
	            max = jData[i].id;
	         }
	             }
	            var partners = {};
				partners.id = max + 1;
				partners.name = companyName;
				partners.contactname = personName;
				partners.email = companyEmail;
				partners.ev = eventsNames;
				partners.img = logo+".jpg";
				//console.log(partners);
				var aPartners =  getDataFromLocalStorageAsObject();	
				aPartners.push(partners);
				var sFinal = JSON.stringify(aPartners);
				//console.log(sFinal);	
				localStorage.sapartners = sFinal;
				$("#img-no").hide();
				$("#img-ok").show();
			    showModalFail('Partner was created succesfully!');
	            deleteValueInputs();



			}
	}
	else {
			$("#img-no").show();
			$("#img-ok").hide();
	         showModalFail('Please fill up all required fields or if all fields checked are filled corect');;

	}
			
	}

	function displayPartners(e){
		// take the localStorage and make it into an object
				var ajData = getDataFromLocalStorageAsObject();
			

				//$("#data").empty();
				for(var i = 0; i < ajData.length; i++ )
				{
	var sDiv = '<div class="col-sm-4 text-center"><div class="logo"><img class="img-responsive" src="img/' + ajData[i].img + '" alt=""><div class="overlayView"><button type="button" class="read" id= "'+ajData[i].id+'">Read more</button></div></div><button type="button" class="btn-edit" id= "'+ajData[i].id+'">Edit/Delete</button></div>';
					// append to the main div data
				  $(".partnersGallery").append(sDiv);
				                                        
	}


	}

	  displayPartners();
	  $(".btn-edit").on("click",function() { 
	  	var ajData = getDataFromLocalStorageAsObject();		
	  	var $modal = (this.id);
	  	  var url = "partners_add.html?name=" + encodeURIComponent($modal);
	  	   window.location.href = url;
	});
	   $(".read").on("click",function() { 
	   	    $('.Company').empty();
	        $('.name').empty();
	        $('.email').empty();
	        $('.event').empty();
	    var $id = (this.id);
	    var jData = getDataFromLocalStorageAsObject();	

	    	for (var i = 0; i < jData.length; i++){

	             if(jData[i].id == $id){

	               $('.Company').append("<span>Company name:</span> "+ jData[i].name);
	               $('.name').append("<span>Contact Person name:</span> " + jData[i].contactname);
	               $('.email').append("<span>Email:</span> " + jData[i].email);
	               $('.event').append("<span>Event: </span> "+ jData[i].ev);

	               $("#Partners").modal('show');
	               break;


	             
	               }
	    	}

	    });

	function showModalFail(text) {
	    $(".modalText").text(text);
	    $("#modalPartnerFail").modal('show');

	}


	$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results == null){
	    
	       return null;
	    }
	    else{
	 $("#btnAdd").remove();
	$("#form-group-buttons").append('<button id="btnDelete" class="btn btn-danger btn-delete-custom">Delete</button>');
	$("#form-group-buttons").append('<button id="btnEdit" class="btn btn-primary">Edit</button>');
	       return results[1] || 0;
	    }
	}
	var linkValue= decodeURIComponent($.urlParam('name'));


	function editing(value){
		var ajData = getDataFromLocalStorageAsObject();		
	   for(var i = 0; i < ajData.length; i++ )
				{  
					if(ajData[i].id == value){
					var companyName = $('.companiesNames').val(ajData[i].name)
	                var personName = $('#NameTxt').val(ajData[i].contactname);//Company email
	                var companyEmail = $('#emailTxt').val(ajData[i].email);
	                var eventsNames = $('.eventsTxt').val(ajData[i].ev);
	                var image =ajData[i].img;
	                var newImage = image.split(".jpg");
	                var logo = $('#logoTxt').val(newImage[0]);
	                break;

					}

				}
		


	}

	function deleteFrom(value) {

	var ajData = getDataFromLocalStorageAsObject();	
		 
	 for(var i = 0; i < ajData.length; i++) {
	    if(ajData[i].id == value) {
	       ajData.splice(i, 1);
	   	var sFinal = JSON.stringify(ajData);
		localStorage.sapartners = sFinal;
	       deleteValueInputs();
	       window.location = "partners_add.html"
	       break;
	    }
	  
	}	// body...
	}


	 editing(linkValue);
	  $("#btnEdit").on("click",function() { 
	  	var companyName = $('.companiesNames option:selected').text();
	//Contact person name
	    var personName = $('#NameTxt').val();
	//Company email
	     var companyEmail = $('#emailTxt').val();
	//Events
	    var eventsNames = $('.eventsTxt option:selected').text();
	   //Select a logo
	   var logo = $('#logoTxt option:selected').text();
	   		//checks for empty input fields in the form
	var empty = $(".flex-item").parent().find("input").filter(function() {
				return this.value === "";
			});
	   if(companyName == logo && !empty.length){

		var jData = getDataFromLocalStorageAsObject();		
		for (var i = 0; i < jData.length; i++){
			if (jData[i].id==linkValue){
				jData[i].name = companyName;
				jData[i].contactname = personName;			
				jData[i].email = companyEmail;
				jData[i].ev = eventsNames;
				jData[i].img = logo+".jpg";
			var sFinal = JSON.stringify(jData);
		        localStorage.sapartners = sFinal;
		        showModalFail('Data was succesfully changed !')

		        break;
			}
		}
	}

	});


	 $("#btnDelete").on("click",function() { 
	deleteFrom(linkValue);
	  });


	////check partners array in local storage
		function getDataFromLocalStorageAsObject(){
				try{
					return JSON.parse(localStorage.sapartners);
				}
				catch(e){
					return [];
				}
			}

	});

